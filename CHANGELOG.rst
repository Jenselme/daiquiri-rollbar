Changelog
=========

0.2.1 (2017-08-15)
------------------

- Improve documentation


0.2.0 (2017-08-15)
------------------

- Log messages into rollbar
